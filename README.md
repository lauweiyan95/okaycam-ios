# OkayCam

![Version](https://img.shields.io/cocoapods/v/OkayCam.svg?style=flat)
![Platform](https://img.shields.io/cocoapods/p/OkayCam.svg?style=flat)

Requires iOS 9+ and Swift 5.

## Contents

- [Installation](#installation)
- [Usage](#usage)
  - [Document Capture](#document-capture)
    - [Configuration](#configuration)
  - [Face Capture](#face-capture)
    - [Configuration](#configuration-1)
  - [Base64 Conversion](#base64-conversion)
- [Error Handling for Document/Face Capture](#error-handling-for-documentface-capture)
- [Example Project](#example)

## Installation

OkayCam is available through [CocoaPods](https://cocoapods.org). To install
it, add the following to your app target in your podfile:

```ruby
  use_frameworks!
  pod 'OkayCam'

  post_install do |installer|
    installer.pods_project.targets.each do |target|
      if target.name == "CryptoSwift"
        puts "Enable module stability for CryptoSwift"
        target.build_configurations.each do |config|
            config.build_settings['BUILD_LIBRARY_FOR_DISTRIBUTION'] = 'YES'
        end
      end
    end
```

You will need to add `NSCameraUsageDescription` to your `info.plist` for camera permissions.

## Usage

Start by importing `OkayCam` module into your swift file.

```swift
import OkayCam
```

### Document Capture

1- Instantiate the configuration object. You must pass a reference to your navigation controller.

```swift
let config = OkayCamConfig(navigationController: nav)
```

The configuration object is initialized with default values.

2- Start the process by calling `start` on `OkayCamDoc` class. You can handle the result in the completion handler.

```swift
OkayCamDoc.start(
    okayCamConfig: config,
    license: yourLicense,
    { filePaths, error in
        // handle
    }
)
```

You will only either receieve `filePaths` or an `error`. One will be valid and the other will be `nil`.

#### Configuration

Modifying the configuration object:

```swift
        let config = OkayCamConfig(navigationController: nav)
        config.topLabel.text = "top label text"
        config.topLabel.color = .green
        config.topLabel.size = 14
        config.bottomLabel.text = "bottom label text"
        config.bottomLabel.color = .systemPink
        config.bottomLabel.size = 20
        config.frame.color = .blue
        config.frame.size = CGSize(width: 230, height: 100)
        config.frame.content = URL(fileURLWithPath: Bundle.main.path(forResource: "image", ofType: "svg")!)
        config.showOverlay = false
        config.timer.backgroundColor = .magenta
        config.timer.textColor = .systemTeal
        config.captureBtnColor = .white
        config.torchBtnEnabled = false
        config.confirmBtnConfig.backgroundColor = .blue
        config.confirmBtnConfig.contentColor = .black
        config.retakeBtnConfig.backgroundColor = .orange
        config.retakeBtnConfig.contentColor = .black
        config.captureConfigPair = CaptureConfigPair(
            firstPhoto: OkayCamCaptureConfig(
                timeOut: 2,
                onFlash: false,
                outputPath: myCustomFilePath
            ),
            secondPhoto: OkayCamCaptureConfig(
                timeOut: 1,
                onFlash: false,
                outputPath: myCustomFilePath
            )
        )
        config.crop = true
        config.width = 250
        config.imageQuality = 1.0
```

Properties on `OkayCamConfig`:

| Property          | Default                                                                                                              | Type                 |
| :---------------- | :------------------------------------------------------------------------------------------------------------------- | :------------------- |
| topLabel          | `OkayCamLabelConfig(text: "Tap to focus", color: UIColor.white, size: CGFloat(24))`                                  | `OkayCamLabelConfig` |
| bottomLabel       | `OkayCamLabelConfig(text: "Please align ID card within frame", color: UIColor.white, size: CGFloat(24))`             | `OkayCamLabelConfig` |
| frame        | `OkayCamFrameConfig(size: nil, color: .white, content: nil)`                                                                                                      | `OkayCamFrameConfig`            |
| showOverlay        | `true`                                                                                                      | `Bool`            |
| timer             | `OkayCamTimerConfig(backgroundColor: UIColor.black, textColor: UIColor.white)`                                       | `OkayCamTimerConfig` |
| captureBtnColor   | `UIColor(red: 0.92, green: 0.08, blue: 0.30, alpha: 1.0)`                                                            | `UIColor`            |
| torchBtnEnabled              | `false`                                                                                                              | `Bool`               |
| confirmBtnConfig | `OkayCamBtnConfig(backgroundColor: UIColor(red: 0.92, green: 0.08, blue: 0.30, alpha: 1.0), contentColor: UIColor.white)` | `OkayCamBtnConfig`  |
| retakeBtnConfig | `OkayCamBtnConfig(backgroundColor: UIColor(red: 0.92, green: 0.08, blue: 0.30, alpha: 1.0), contentColor: UIColor.white)` | `OkayCamBtnConfig`  |
| captureConfigPair | `CaptureConfigPair(firstPhoto: OkayCamCaptureConfig(timeOut: 0, onFlash: false, outputPath: nil), secondPhoto: nil)` | `CaptureConfigPair`  |
| crop              | `false`                                                                                                              | `Bool`               |
| width             | `nil`                                                                                                                | `Int?`               |
| imageQuality             | `1.0 (range 0 - 1.0)`                                                                                                                | `CGFloat`               |
| frameSize         | `nil`                                                                                                                | `CGSize?`            |

### Face Capture

1- Instantiate the configuration object. You must pass a reference to your navigation controller.

```swift
let config = OkaySelfieConfig(navigationController: nav)
```

2- Start the process by calling `start` on `OkayCamSelfie` class. Similarly, you can handle the result in the completion handler.

```swift
OkayCamSelfie.start(
    okaySelfieConfig: config,
    license: yourLicense,
    { filePath, error in
        // handle
    }
)
```

You will only either receieve the `filePath` or an `error`. One will be valid and the other will be `nil`.

#### Configuration

Modifying the configuration object:

```swift
        let config = OkaySelfieConfig(navigationController: nav)
        config.topLabel.text = "top label text"
        config.topLabel.color = .green
        config.topLabel.size = 14
        config.bottomFrameColor = .darkGray
        config.captureBtnColor = .white
        config.swichBtnConfig.show = true
        config.swichBtnConfig.color = .magenta
        config.confirmBtnConfig.backgroundColor = .blue
        config.confirmBtnConfig.contentColor = .black
        config.retakeBtnConfig.backgroundColor = .orange
        config.retakeBtnConfig.contentColor = .black
        config.width = 250
        config.imageQuality = 1.0
        config.defaultCameraFacing = .back
        config.outputPath = myCustomFilePath
```

Properties on `OkaySelfieConfig`:

| Property            | Default                                                                                                           | Type                    |
| :------------------ | :---------------------------------------------------------------------------------------------------------------- | :---------------------- |
| topLabel            | `OkaySelfieLabelConfig(text: "Please align your face within the frame", color: UIColor.white, size: CGFloat(18))` | `OkaySelfieLabelConfig` |
| bottomFrameColor    | `UIColor(red: 0.92, green: 0.40, blue: 0.04, alpha: 1.0)`                                                         | `UIColor`               |
| captureBtnColor     | `UIColor(red: 0.92, green: 0.08, blue: 0.30, alpha: 1.0)`                                                         | `UIColor`               |
| swichBtnConfig      | `SwitchBtnConfig(color: UIColor.white, show: false)`                                                              | `SwitchBtnConfig`       |
| confirmBtnConfig | `OkayCamBtnConfig(backgroundColor: UIColor(red: 0.92, green: 0.08, blue: 0.30, alpha: 1.0), contentColor: UIColor.white)` | `OkayCamBtnConfig`  |
| retakeBtnConfig | `OkayCamBtnConfig(backgroundColor: UIColor(red: 0.92, green: 0.08, blue: 0.30, alpha: 1.0), contentColor: UIColor.white)` | `OkayCamBtnConfig`  |
| defaultCameraFacing | `OkayCam.CameraDevice.front`                                                                                      | `OkayCam.CameraDevice`  |
| frameColor          | `UIColor.black`                                                                                                   | `UIColor`               |
| outputPath          | `nil`                                                                                                             | `URL?`                  |
| width               | `nil`                                                                                                             | `Int?`                  |
| imageQuality             | `1.0 (range 0 - 1.0)`                                                                                                                | `CGFloat`               |

### Base64 Conversion

As long as you have imported `OkayCam` module, then you have access to the following function:

```swift
do {
    let base64String = try convertImageToBase64(fileUrl: myImageFilePath)
    print(base64String)
} catch  {
    print(error.localizedDescription)
}
```

## Error Handling for Document/Face Capture

The error received in the completion handler for both document/face capture is of the type `OkayCamError`:

```swift
enum OkayCamError: Error {
    // No error at all.
    case noError
    // Error due to invalid parameter value in config object.
    case invalidConfig(_ msg: String)
    // Error in capturing image.
    case imageCapture(_ msg: String)
    // Error due to invalid license usage.
    case invalidLicense
    // Error due to user clicking cancel and going back.
    case userCancelled
    // Error due to camera permission denied by user.
    case cameraPermission
    // Error due to camera device.
    case camera(_ msg: String)
    // Any other errors.
    case miscellaneous(_ msg: String)
}
```

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

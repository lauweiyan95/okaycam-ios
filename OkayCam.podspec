Pod::Spec.new do |s|
  s.name             = 'OkayCam'
  s.version          = '1.1.4'
  s.license          = { :file => 'LICENSE', :type => 'Commercial' }
  s.author           = { 'Raymond' => 'raymond@innov8tif.com' }
  s.homepage         = 'https://innov8tif.com'
  s.summary          = 'OkayCam iOS SDK.'
  s.source           = { :git => 'https://gitlab.com/innov8tif-ekyc-product/cocoapods/okaycam-ios', :tag => 'v1.1.4', :branch => 'master' }
  
  s.ios.deployment_target = '9.0'
  s.vendored_frameworks = 'OkayCam.xcframework'
  
  s.swift_version = '5.0'
  s.dependency 'CryptoSwift', '~> 1.3.2'
end
